# UChat聊天工具	

## 简介
利用HTML5课程所学内容进行开发的类似微信的实时聊天工具

* 前台界面开发利用**Ionic+JQuery**
* 后台基于**node.js**，使用了`express`和`express-ws`
* 使用**cordova**调用了摄像头模块，使用[**intel XDK**](http://https://software.intel.com/en-us/intel-xdk)
* 
## Team2 members：冷若冰 刘知达 张浩然
### Work

* 刘知达：Server,Debug,Self-challenge:Group chat
* 冷若冰：Client UI,Self-challenge：Push to heroku 
* 张浩然：Login UI,Git,Send picture,doc,Self-challenge：Push to heroku

##文件说明
www下为项目的client代码，为index.html为入口

server下为服务器代码，其中server.js为服务器代码

plugins中为cordova的插件
## 页面设计

### 基本外观
* **Header**：使用Ionic中的`bar bar-header`类，上面有初始额WeChat标志以及按钮，进入不同的功能后内容会随之改变

* **Navigation Bar**：使用`tabs tabs-icon-top`类，上面有不同界面的按钮，点击后会跳转到不同的界面

* **Content View**：`content has-header`类， 对应Navigation Bar中的不同`item`，显示相应的内容（me和setting功能不全）

* **Chat Details View**：中间有一个空白的`content`，能够正确显示自己和对方发出的内容，在底部提供消息输入框、“发送”按钮和其他功能按钮；

* **Log In**： 使用规定的用户名和密码进行登录的界面*（因为不具备注册功能，所以忘记密码功能没有添加）*

### 特殊亮点

* 聊天列表的右侧在有未读消息时会有一个未读消息提示，为`badge`类，颜色是
红色，其中的数字显示未读消息数
![](http://i.imgur.com/mhvc1iQ.png)
* 聊天界面具有表情键盘，可以在聊天中收发表情
* ![](http://i.imgur.com/nZSEAVx.png)
## 功能介绍
###登录功能
进入软件会首先看到登录界面，只有使用正确的用户名和密码才可以进入功能界面，任意一项输入错误都会报错，以用户名错误优先
![](http://i.imgur.com/eEjADm2.png)
###聊天功能
在聊天界面下具备以下功能：

* **基础的文字聊天功能**:所有发出和收到的消息都会原封不动的连同发送人等信息一同保存，每次进入一个人的聊天界面都会读取所有相关的消息并显示，达到还原了聊天状态的结果


* **未读消息提醒**：收到信息时如果当前界面不是该信息的接受界面，就将对应聊天的未读消息数加1并显示，在进入该聊天界面时数值清空并隐藏


* **离线消息推送**：在登陆后服务器会将离线阶段收到的所有消息推送


* **表情发送**：通过对表情图标编码并替换的方式实现


* **图片发送功能**：*(调用自cordova的Camera Plugin)*



###登出功能
利用settings中的logout按钮来实现用户的登出，返回登录界面

## Client/Server通信设计

所有的信息均以JSON格式传输

###Client部分
####登陆
登录按钮点击时首先建立`websocket`连接，并在经过0.2s的等待时间*（等待WS建立连接）*后将`username`和`password`中的内容传输至服务器，服务器会返回一个`LogState`，根据其结果来确定是有错误还是登陆成功。
#####发信息
发送信息中包括了`from`，`to`，`data`，`image`，四个部分，分别表示消息的发送人，接收人，消息内容和附带图片，发出后保存在本地的消息记录中
####收消息
收到的消息会带有`from`，`data`两部分，客户端首先会保存这个消息，然后根据`from`来决定是推送到当前聊天页面还是提示未读消息

###Server部分
####登陆
收到用户的登录请求之后服务器会在本地的存储中搜索符合的用户名和密码，如果不符合返回错误信息，如果符合返回成功信息，将当前用户的WebSocket连接保存并立即推送离线消息队列中送给该用户的消息
####消息转发
服务器收到的信息类型除了登录信息全部都是含有`from`，`to`，`data`，`image`的`JSON`消息，首先服务器会判断to的对象是否在WebSock列表中，如果不在的话说明该消息为离线消息，将之推入离线消息队列；如果在线则将`from`，`data`，`image`这三项发送给对应的联系人
###登出
当一个WebSocket被断开的时候，服务器删除对应列表中的项，完成账户的登出
>群聊功能：
>带有group标签的消息，会被转发给对应成员列表中除了发送者以外的所有人，`from`变成群聊名称，`data`的最开始加上消息发送者的名字，达到群聊中区分不同人的效果
>
>此功能不完善


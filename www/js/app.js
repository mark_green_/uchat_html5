var ws;
var Messages = [];
var onChat;

function start () {
	$(function() {
       
        $('.log-in').show();
        $('.chatuse').hide();
       $('.mainPage').hide();
        $(".badge").hide();
        
       logIn();
         sendButtonClick();
            returnButtonClick();
            showemotion();
            sendemotion();
            bindNaviTabEvents ();
            showsettings();
            imageButton();
            addgroupChat();
         showChatPage ();
	});
}
function logIn(){ 

    $('#sign-in-user').click(function(){
    ws = new WebSocket('ws://localhost:3000/chat');
     window.setTimeout(sendId, 200);
      ws.onmessage = WSonmessage;
    return ws;
    
    });

}                                                  //the function of login and password checking


                                                   //send the message via websocket
 function sendId(){
        ws.send(JSON.stringify({
        id: $('#user-name').val(),
        password:$('#pass-word').val()
      }));
  }
 function WSonmessage(event){
    var messageOut = event.data;
    var obj = JSON.parse(messageOut);
    if("logState" in obj){
        if(obj.logState == "username"){
            alert("wrong username!")
            ws.close();
        }
        else if (obj.logState=="password"){
            alert("wrong password!");
            ws.close();
        }
        else if(obj.logState=="success"){
             $('.log-in').hide();
            $('.mainPage').show();
            $('#'+$('#user-name').val()).hide();
            $('#'+$('#user-name').val()+'-contact').remove();
            showTab('WeChat');
           
           
            //stickToTop();
        }
    }
    else{
        Messages.push(obj);
        if(onChat!=null){
            if(obj.from == onChat){
                console.log(obj+obj.data);
                addReceivedMessage(obj.data,obj.image);
            }
        }
        else{
                var tempmsg=$('#'+obj.from+'-badge').html();
            tempmsgCount = parseInt(tempmsg);
            if(tempmsgCount==0)
            {
                $('#'+obj.from+'-badge').show();
                $('#'+obj.from+'-badge').html(1);
            }
            else{
                 $('#'+obj.from+'-badge').html(tempmsgCount+1);
            }
        }
        }
   }

//function stickToTop(){-------------------------------taphold function
//    $(".item-avatar").on("taphold",function(){
//  $(this).hide();
//});
//}
function addgroupChat(){
    $('#addGroupChat_button').click(function (){
     
       $('.content').hide();
        $('#addGroupChat').show();
           $('#backButton').show();
    });
}

function imageButton(){
    $("#imagebutton").on("click", function() {
        console.log('click me ')
        navigator.camera.getPicture(onSuccess, onFail, { quality: 50,
    destinationType: navigator.camera.DestinationType.DATA_URL
});

function onSuccess(imageData) {
    var image = document.getElementById('captured-image-placeholder');
    image.src = "data:image/jpeg;base64," + imageData;
      $(".picture-input-area").show();
}
function onFail(message) {
alert('Failed because: ' + message);
}
     

});
   $(".picture-input-area img").on("click", function(evt) {
        var confirmation = confirm("删除图片吗？");
        if (!confirmation) return;
        $("#captured-image-placeholder").attr("src", "");
        $(".picture-input-area").hide();
    }); 
}
function readMessage(){
    
    for(var i = 0; i <= Messages.length-1; i++) {
        if("to" in Messages[i]){
            if(Messages[i].to==onChat){
                addSendMessage(Messages[i].data,Messages[i].image);
                console.log(Messages[i]);
            }
        }
        else{
            if(Messages[i].from==onChat){
                addReceivedMessage(Messages[i].data,Messages[i].image);
                console.log(Messages[i]);
            }
        }
    }
}


function showTab (tabId) {
	// Hide all content tab.
	$('.content').hide();

	// Show the specified content.
	$('#' + tabId + '-content').show();
    $("h1").html(tabId);
}                                                //show the right content



function showChatPage () {
	
	$('.item-avatar').click(function () {
        
        $('#show-emotion').hide();
        onChat = $(this).attr('id');
         $('#addGroupChat_button').hide();
        var contactId = $(this).find("h2").html();
		$('.content').hide();
        $(".chatuse").show();
       $("h1").html("chat from "+contactId);
        clearUnread(onChat);
        readMessage();
   returnButtonClick();
        
	});
    $('.item-thumbnail-left').click(function(){
        onChat = $(this).attr('id');
           $('#addGroupChat_button').hide();
        var contactId = $(this).find("h2").html();
        $('.content').hide();
        $(".chatuse").show();
       $("h1").html("chat from "+contactId);
         clearUnread(onChat);
   returnButtonClick();
        readMessage();
    });
}                                           //show the detail of the chat if a contact is click

function clearUnread(ID){
     $('#'+ID+'-badge').hide();
                $('#'+ID+'-badge').html(0);
}

function sendButtonClick(){
$('#sendbutton').click(function () {
    var messageOut = $('#sendBox').val();
    var imageURI = $("#captured-image-placeholder").attr("src");
    if(messageOut != ''||imageURI!=""){
            var toSendObj = JSON.stringify({
                from:$('#user-name').val(),
                to: onChat,
                data: messageOut,
                image:imageURI
            })
            Messages.push(JSON.parse(toSendObj));
        console.log(toSendObj);
            ws.send(toSendObj);
        addSendMessage(messageOut,imageURI);
       $('#sendBox').val('');
        $("#captured-image-placeholder").attr("src", "");
            $(".picture-input-area").hide();
    }
    else{
    console.log("there is no text yet!");
    }

});
    
}                                           // send button click 

function showsettings()
{
    $('#settings').click(function(){
        
        $('#Me-content').hide();
        $('#logout').show();
        logOut();
        
    });
    
}
function logOut(){
    $('#logout_button').click(function(){
            ws.close();
             $('#user-name').val('');
        $('#pass-word').val('');
         $('.item').show();
            $('.log-in').show();
            $('.chatuse').hide();
            $('.mainPage').hide();
        Messages=[];
        });
}
function addReceivedMessage(messageOut,imageURI){
    if(messageOut!=''){
    messageOut1 = messageOut.replace(/-happy-/g, '<i class="icon ion-android-happy" ></i>');
    messageOut2 = messageOut1.replace(/-sad-/g, '<i class="icon ion-android-sad" ></i>');
    
$('#chat-use-content').append('<div class="leftd"><div class="leftimg">'+'<img src="' + (imageURI || "") + '">' +'</div> <div class="speech left" > '+messageOut2+' </div></div>');  
$('#chat-use-content').scrollTop(500);
    }
    else{
        $('#chat-use-content').append('<div class="leftd"><div class="leftimg">'+'<img src="' + (imageURI || "") + '">' +'</div></div>');  
        $('#chat-use-content').scrollTop(500);
    }
}
function addSendMessage(messageOut,imageURI){
     if(messageOut!=''){
    messageOut1 = messageOut.replace(/-happy-/g, '<i class="icon ion-android-happy" ></i>');
    messageOut2 = messageOut1.replace(/-sad-/g, '<i class="icon ion-android-sad" ></i>');
    
$('#chat-use-content').append('<div class="rightd"><div class="rightimg">'+'<img src="' + (imageURI || "") + '">' +'</div> <div class="speech right" > '+messageOut2+' </div></div>');  
$('#chat-use-content').scrollTop(500);
     }
    else{
        $('#chat-use-content').append('<div class="rightd"><div class="rightimg">'+'<img src="' + (imageURI || "") + '">' +'</div></div>');  
$('#chat-use-content').scrollTop(500);
    }
}                                                                   //send message to the chat-screen                       




function  returnButtonClick(){

    $('#backButton').click(function () {
    $('#chat-use-content').empty();
     $('.chatuse').hide();
        $('.content').hide();
        showTab('WeChat');
        onChat = null;
        $('#addGroupChat_button').show();
    });
    
}                                                                   //return from the chatpage to the main page


function showemotion(){

    $('#emotion').click(function(){
        
        
        $('#show-emotion').toggle();
        $('#show-emotion').css('position','absolute');
       // $('#show-emotion').css('top','-100px');
        $('#show-emotion').css('left','0px');
        $('#show-emotion').css('bottom','70px');
});

}                                                                  //show the emotion selector

function sendemotion(){
 $('#happy').click(function(){
     var a = '-happy-';
     $('#sendBox').val($('#sendBox').val()+a);
 
 });
$('#sad').click(function(){
    var a = '-sad-';
    $('#sendBox').val($('#sendBox').val()+a);
});
    
}                                                                    //add the emotion in the text box         



    
function bindNaviTabEvents () {
	
	$('.mainTab').click(function () {

		var tabId = $(this).attr('id');

		showTab(tabId);

	});
}                                                              //responce to the click on the main tab and switch the content



start();